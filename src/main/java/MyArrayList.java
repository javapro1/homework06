import java.util.Arrays;

public class MyArrayList<T> implements MyList<T> {
    @Override
    public String toString() {
        return "MyArrayList{" +
                "arr=" + Arrays.toString(arr) +
                '}';
    }

    private static final int INITIAL_SIZE = 8;
    private Object[] arr = new Object[INITIAL_SIZE];
    private int size = 0;

    public void validateIndex(int index) {
        if (index < 0 || index > arr.length) {
            throw new IndexOutOfBoundsException();
        }
    }

    @Override
    public void add(T elem) {
        arr = Arrays.copyOf(arr, arr.length + 1);
        arr[arr.length - 1] = elem;
    }

    @Override
    public void insert(T elem, int index) {
        validateIndex(index);
        arr = Arrays.copyOf(arr, arr.length + 1);
        System.arraycopy(arr, index, arr, index + 1, arr.length - index - 1);
        System.arraycopy(arr, 0, arr, 0, index);
        arr[index] = elem;
        System.out.println(Arrays.toString(arr));
        System.out.println(arr.length);
    }

    @Override
    public void remove(int index) {
        validateIndex(index);
        for (int i = index; i < arr.length - 1; i++) {
            arr[i] = arr[i + 1];
        }
        arr = Arrays.copyOf(arr, arr.length - 1);
        System.out.println(Arrays.toString(arr));
    }

    @Override
    public void remove(T value) {
        for (int i = 0; i < arr.length; i++) {
            if (value.equals(arr[i])) {
                for (int j = i; j < arr.length - 1; j++) {
                    arr[j] = arr[j + 1];
                }
                arr = Arrays.copyOf(arr, arr.length - 1);
                break;
            }
        }
        System.out.println(Arrays.toString(arr));
    }

    @Override
    public int indexOf(T value) {
        for (int i = 0; i < arr.length; i++) {
            if (value.equals(arr[i])) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public boolean contains(T value) {
        for (int i = 0; i < arr.length; i++) {
            if (value.equals(arr[i])) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean containsAll(MyList<T> values) {
        for (int i = 0; i < values.size(); i++) {
            if(!contains(values.get(i))){
                return false;
            }
        }
        return true;
    }

    @Override
    public T get(int index) {
        validateIndex(index);
        return (T) arr[index];
    }

    @Override
    public void set(int index, T value) {
        validateIndex(index);
        arr[index] = value;
        System.out.println(Arrays.toString(arr));

    }

    @Override
    public int size() {
        return arr.length;
    }
}
