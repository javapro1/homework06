import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        MyArrayList<Integer> list = new MyArrayList<>();
        MyList<Integer> newList = new MyArrayList<>();
        System.out.println("------------Methods----------------");
        list.add(100500);
        list.add(25);
        list.insert(1, 1);
        list.remove(0);
        list.remove(new Integer(100500));
        System.out.println("Index of same value: " + list.indexOf(25));
        System.out.println("Is this value contains in array: " + list.contains(25));
        System.out.println("Is all values contains in array: " + list.containsAll(newList));
        System.out.println("Index of number: " + list.get(3));
        list.set(0, 123456789);
        System.out.println("Length of array: " + list.size());
    }
}
