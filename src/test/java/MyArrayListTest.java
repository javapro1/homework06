import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MyArrayListTest {
    MyArrayList<Integer> list = new MyArrayList<>();
    MyList<Integer> newList = new MyList<Integer>() {
        @Override
        public void add(Integer elem) {

        }

        @Override
        public void insert(Integer elem, int index) {

        }

        @Override
        public void remove(int index) {

        }

        @Override
        public void remove(Integer value) {

        }

        @Override
        public int indexOf(Integer value) {
            return 0;
        }

        @Override
        public boolean contains(Integer value) {
            return false;
        }

        @Override
        public boolean containsAll(MyList<Integer> values) {
            return false;
        }

        @Override
        public Integer get(int index) {
            return null;
        }

        @Override
        public void set(int index, Integer value) {

        }

        @Override
        public int size() {
            return 0;
        }
    };

    @Test
    void validateIndex() {
        int index = 4;
       // assertTrue(list.validateIndex(index));
    }

    @Test
    void add() {
    }

    @Test
    void insert() {
    }

    @Test
    void remove() {
    }

    @Test
    void testRemove() {
    }

    @Test
    void indexOf() {
    }

    @Test
    void contains() {
    }

    @Test
    void containsAll() {
    }

    @Test
    void get() {
    }

    @Test
    void set() {
    }

    @Test
    void size() {
    }
}